# Dagger Templates

You can get up and running with [Dagger](https://dagger.io/) in GitLab by choosing one of the two flavors, `neat` or `mixed`.

To target a specific version of Dagger, import this template with a tag reference. Tags are aligned with the Dagger release cycle.

```yaml
include: https://gitlab.com/purpleclay/templates/-/raw/dagger/0.16.2/dagger/Neat.gitlab-ci.yml
```

Use `main` if you want to stay in sync with the latest.

```yaml
include: https://gitlab.com/purpleclay/templates/-/raw/main/dagger/Neat.gitlab-ci.yml
```

## Neat

This template provides a no-nonsense approach to using Dagger (neat) without any bells or whistles. It is designed to give you full control.

```yaml
include: https://gitlab.com/purpleclay/templates/-/raw/main/dagger/Neat.gitlab-ci.yml

ponysay:
  extends: [.dagger]
  variables:
    DAGGER_MODULE: "github.com/purpleclay/daggerverse/ponysay@v0.2.0"
    DAGGER_ARGS: "inspire-me"
    _EXPERIMENTAL_DAGGER_RUNNER_HOST: "unix:///var/run/buildkit/buildkitd.sock"
  tags:
    - dagger
```

## Mixed

It's less potent than the other template but quickly gets you up and running. Designed for use with the GitLab shared runner, it spins up a temporary Dagger Engine on the fly.

```yaml
include: https://gitlab.com/purpleclay/templates/-/raw/main/dagger/Mixed.gitlab-ci.yml

ponysay:
  extends: [.dagger]
  variables:
    DAGGER_MODULE: "github.com/purpleclay/daggerverse/ponysay@v0.2.0"
    DAGGER_ARGS: "inspire-me"
```

### Dagger Cloud Cache Syncing

Set the `DAGGER_CLOUD_GRACE_PERIOD` environment variable to push all cache layers to the Dagger Cloud. It defaults to five minutes.

## Docker Login Utility

You need to log into docker before running your Dagger function, no problem!

```yaml
say_hello:
  extends:
    - .dagger
    - .docker_login
```

## Calling a Private Dagger Module

If you need to call a private Dagger module, you can extend the `.dagger_private` job and provide the required credentials. Please ensure you [mask](https://docs.gitlab.com/ee/ci/jobs/ci_job_token.html#add-a-group-or-project-to-the-job-token-allowlist) these values!

```yaml
say_hello:
  extends:
    - .dagger_private
  variables:
    DAGGER_MODULE: "github.com/purpleclay/daggerverse/ponysay@v0.2.0"
    DAGGER_ARGS: "inspire-me"
    GIT_USERNAME: $USERNAME
    GIT_PASSWORD: $PASSWORD
```

### Using the GitLab Job Token

You can use the short-lived job token if your private Daggerverse is in GitLab. Just [grant](https://docs.gitlab.com/ee/ci/jobs/ci_job_token.html#add-a-group-or-project-to-the-job-token-allowlist) your project access using its job token.

```yaml
GIT_USERNAME: gitlab-ci-token
GIT_PASSWORD: $CI_JOB_TOKEN
```
