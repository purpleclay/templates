# GitLab Templates

A collection of GitLab templates for your everyday pipelining needs.

- [Dagger](./dagger/README.md): Go forth and daggerize your pipelines.
